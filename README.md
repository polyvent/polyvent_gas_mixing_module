# PolyVent_Gas_Mixing_Module

Current Gas Mixing module is adopted from the SmithVent design: https://grabcad.com/smithvent-1

This module mixes the O2 and medical air at 50 psi down to 
about 22 psi in a proportional way.

It consists of an acrylice tube, capped with aluminum caps held
together with threaded rods.

This design can be constructed almost anywhere even when there are
supply chain problems.